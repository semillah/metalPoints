#include <metal_stdlib> // Include the standard Metal library which provides essential functions and types.

using namespace metal; // Use the Metal namespace to conveniently access its functions and types without prefixing them with 'metal::'.

// Struct 'VertexOut' is used to define the output of the vertex shader.
// This output will be passed on to the next stage of the graphics pipeline, typically a fragment shader.
struct VertexOut {
    float4 position [[position]]; // The position of the vertex in normalized device coordinates.
                                  // The [[position]] attribute qualifier specifies that this variable
                                  // will be used as the position for each vertex in the rendered output.

    float pointSize [[point_size]]; // The size of the point to be rendered, which affects how large
                                    // each point appears on the screen. The [[point_size]] attribute
                                    // qualifier indicates that this variable represents the size of
                                    // the rendered points.

    float4 color; // The color of the vertex. This data will be interpolated across the primitive
                  // being rendered (in this case, points) and passed to the fragment shader.
                  // The color is represented as a four-component vector (red, green, blue, alpha).
};

// 'perm' is a permutation table used for generating pseudo-random values in the Simplex noise function.
// It is a key component in the noise generation process, influencing the randomness and appearance
// of the resulting noise pattern.
constant int perm[256] = {
    // A predefined array of 256 integers. The values and order of this array significantly
    // affect the characteristics of the generated noise. This particular sequence is a common
    // permutation used in Simplex noise generation.
            151, 160, 137, 91, 90, 15,
            131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36,
            103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
            190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117,
            35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87,
            174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71,
            134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111,
            229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55,
            46, 245, 40, 244, 102, 143, 54, 65, 25, 63, 161,
            1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18,
            169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100,
            109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124,
            123, 5, 202, 38, 147, 118, 126, 255, 82, 85, 212,
            207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
            223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163,
            70, 221, 153, 101, 155, 167, 43, 172, 9, 129, 22,
            39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178,
            185, 112, 104, 218, 246, 97, 228, 251, 34, 242, 193,
            238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145,
            235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199,
            106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127,
            4, 150, 254, 138, 236, 205, 93, 222, 114, 67, 29,
            24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
};

// Function 'noiseColor' takes a float value (noise) and generates a color based on it.
// This function is used to assign a unique color to each vertex based on the noise value at its position.
float4 noiseColor(float noise) {
    // Map the input noise value to a range between 0 and 1 for both red and blue components of the color.
    // The sin and cos functions are used to create a smooth transition between values as the noise changes.
    float r = (sin(noise) + 1.0) * 0.5; // Red component influenced by the noise value.
    float b = (cos(noise) + 1.0) * 0.5; // Blue component influenced by the noise value.

    // Create and return a color from deep red to bright magenta.
    // The green component is always set to 0, and the alpha component (transparency) is set to 1 (fully opaque).
    return float4(r, 0.0, b, 1.0); // Constructing the color as a float4 (RGBA).
};

// Function 'grad' calculates a gradient based on a hash value and a 2D coordinate (x, y).
// This function contributes to the 'randomness' in the generated noise by assigning
// different gradient vectors based on the hash and coordinates.
float grad(int hash, float x, float y) {
    // Determine the low 3 bits of the hash code. These bits are used to determine the gradient direction.
    int h = hash & 7;

    // Depending on the value of h, u and v are set to x or y to pick a gradient direction.
    float u = h<4 ? x : y;
    float v = h<4 ? y : x;

    // Construct the gradient value by combining u and v with the signs determined by the hash.
    // This results in 8 possible gradient directions (since h can range from 0 to 7).
    return ((h&1)? -u : u) + ((h&2)? -2.0 * v : 2.0 * v);
};


// 2D Simplex noise function
float simplex_noise(float2 v) {
    // Constants 'C' and 'D' are used in the Simplex noise calculation.
    // They are derived from the fact that the simplex grid is equilateral, and these values help to
    // find the skewing and unskewing factors for 2D space. They are precomputed for efficiency.
    const float C = 0.5 * (sqrt(3.0) - 1.0);
    const float D = (3.0 - sqrt(3.0)) / 6.0;
    
    // First corner: This section computes the simplex cell in which the input point 'v' lies.
    // Skewing is applied to determine which simplex cell we are in.
    float2 i  = floor(v + (v.x + v.y) * C); // Skewing the input space to determine which simplex cell we're in
    float2 x0 = v - i + (i.x + i.y) * D; // Unskewing factor, and work out the x,y distances from the cell origin

    // Other two corners: Determining the other two corners of the simplex in 2D.
    float2 i1;
    // Depending on the position of x0, we decide which direction to move for the second corner.
    if(x0.x > x0.y) {
        i1.x = 1.0; i1.y = 0.0;
    } else {
        i1.x = 0.0; i1.y = 1.0;
    }
    // The second and third corners are computed relative to the first one using the unskewing factor D.
    float2 x1 = x0 - i1 + D; // Offsets for middle corner in (x,y) unskewed coords
    float2 x2 = x0 - 1.0 + 2.0 * D; // Offsets for last corner in (x,y) unskewed coords

    // Permutations: Applying a permutation table to the coordinates to remove visual artifacts.
    // This introduces randomness to the gradient selection process.
    i = fmod(i, 256.0); // Wrapping the index to stay within the permutation table size
    int gi0 = perm[int(i.x) + perm[int(i.y)]] % 8;
    int gi1 = perm[int(i.x + i1.x) + perm[int(i.y + i1.y)]] % 8;
    int gi2 = perm[int(i.x + 1.0) + perm[int(i.y + 1.0)]] % 8;

    // Calculate the contribution from the three corners: Each corner of the simplex contributes to the
    // final noise value. The contribution is based on the distance to the corner.
    float t0 = 0.5 - x0.x*x0.x - x0.y*x0.y;
    float t1 = 0.5 - x1.x*x1.x - x1.y*x1.y;
    float t2 = 0.5 - x2.x*x2.x - x2.y*x2.y;

    float n0, n1, n2; // Noise contributions from the three corners

    // Calculate the noise contributions from the three corners: If the point is on the edge of the simplex,
    // its contribution is zero. Otherwise, the contribution is a function of the distance to the edge.
    if(t0<0) {
        n0 = 0.0; // The point is outside the simplex, contributing nothing
    } else {
        t0 *= t0; // Squaring the distance
        n0 = t0 * t0 * grad(gi0, x0.x, x0.y); // Calculating the gradient contribution
    }

    // Repeat the process for the second corner
    if(t1<0) {
        n1 = 0.0;
    } else {
        t1 *= t1;
        n1 = t1 * t1 * grad(gi1, x1.x, x1.y);
    }

    // Repeat the process for the third corner
    if(t2<0) {
        n2 = 0.0;
    } else {
        t2 *= t2;
        n2 = t2 * t2 * grad(gi2, x2.x, x2.y);
    }

    // Sum and scale the result to cover the range [-1,1]. This is the final noise value.
    // The value 70.0 is a scaling factor to adjust the range of the noise values.
    // It's often determined experimentally to achieve the desired visual effect.
    return 70.0 * (n0 + n1 + n2);
}


// Vertex shader: This function is called for each vertex in the buffer.
vertex VertexOut vertex_main(constant float2 *vertices [[buffer(0)]],
                             constant float &time [[buffer(1)]],
                             uint vid [[vertex_id]]) {
    VertexOut vertOut; // Output structure to pass to the fragment shader.

    // Retrieve the original position of the vertex
    // Each vertex's position is passed in as an array, and we use the vertex id to access each one.
    float2 originalPosition = vertices[vid];

    // Dynamic parameters to alter the noise pattern over time, creating animation.
    // 'dynamicFrequency' changes the frequency of the noise, affecting the 'tightness' of the pattern.
    // 'dynamicSize' changes the magnitude of the vertex displacement, making the effect more or less pronounced.
    float dynamicFrequency = 1.0 + sin(time * 0.2) * 0.5; // Oscillates between 0.5 and 1.5
    float dynamicSize = 0.02 + sin(time * 0.3) * 0.01; // Oscillates between 0.015 and 0.025

    // Simulate a dynamic permutation by offsetting indices, adding further variation over time.
    float permOffset = sin(time * 0.1) * 10.0;

    // Generate layered noise with different attributes to create a more complex and organic pattern.
    // 'noiseBase' is the primary noise affecting the vertices.
    // 'noiseDetail' adds a second layer of noise with a different frequency and offset for additional complexity.
    float noiseBase = simplex_noise(originalPosition * dynamicFrequency + float2(time * 0.1, time * 0.1));
    float noiseDetail = simplex_noise((originalPosition + permOffset) * (dynamicFrequency * 2.0) + float2(time * 0.2, time * 0.2));

    // Combine base noise and detailed noise for the final effect.
    // The weights (0.6 and 0.4) determine the influence of each layer of noise.
    float combinedNoise = noiseBase * 0.6 + noiseDetail * 0.4;

    // Convert the combined noise value to a smooth directional angle (in radians).
    float angle = 2.0 * 3.14159265 * combinedNoise;

    // Convert the angle to a direction vector for displacing the vertex.
    // This is where the vertices get their 'movement'.
    float2 direction = float2(cos(angle), sin(angle));

    // Update the position of the vertex based on the noise-driven direction.
    // 'dynamicSize' controls how far each vertex moves in this direction.
    float2 newPosition = originalPosition + dynamicSize * direction;

    // Set the position of the vertex in the output.
    // The 'position' attribute is special and used by the rasterizer to place pixels on the screen.
    vertOut.position = float4(newPosition, 0.0, 1.0);

    // Set the size of the points. This will be used by the rasterizer when expanding points into pixel quads.
    vertOut.pointSize = 2.0;

    // Assign a color to the vertex based on the noise value.
    // The 'noiseColor' function maps the noise value to a color spectrum.
    vertOut.color = noiseColor(combinedNoise);

    // Return the modified vertex output, which will be used by the next stage in the pipeline (the fragment shader).
    return vertOut;
}

// Fragment shader: This function is called for each fragment (potential pixel) the rasterizer produces.
fragment float4 fragment_main(VertexOut vertOut [[stage_in]]) {
    // Simply pass through the color assigned in the vertex shader.
    // This could be further modified or combined with other data if needed.
    return vertOut.color;
}

