//
//  metalPointsApp.swift
//  metalPoints
//
//  Created by Raul on 12/23/23.
//

import SwiftUI
import MetalKit
import CoreMIDI

@main
struct metalPointsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
