import SwiftUI
import MetalKit
import CoreMIDI


// MidiManager is responsible for handling MIDI interactions. It's an ObservableObject,
// which means it can be used within SwiftUI views to automatically update the view
// when any @Published properties change. This is part of SwiftUI's data flow system
// and allows for responsive and dynamic UIs that reflect the current state of the application.
class MidiManager: ObservableObject {
    // midiClient is a reference to a MIDI client. A MIDI client is an object that
    // represents your application in the MIDI system. You interact with the MIDI system
    // by sending and receiving MIDI messages through this client.
    var midiClient: MIDIClientRef = 0
    
    // inPort is a reference to a MIDI input port. Input ports are used to receive
    // MIDI messages from external sources. When you create an input port, you provide
    // a callback that is called whenever there are incoming MIDI messages.
    var inPort: MIDIPortRef = 0
    
    // @Published is a property wrapper provided by SwiftUI. It's used here
    // to automatically notify the UI to update whenever the noiseIntensity changes.
    // noiseIntensity represents the current value that is controlled by MIDI messages,
    // in this case, likely altering the visual or audio output of your app.
    @Published var noiseIntensity: Float = 0.4
    
    // The initializer is called when a new instance of MidiManager is created.
    // It's responsible for setting up the MIDI environment by calling setupMIDI.
    init() {
        setupMIDI()
    }
    
    // setupMIDI is responsible for initializing the MIDI client, creating an input port,
    // and connecting to available MIDI sources. This sets up the necessary infrastructure
    // to receive and process MIDI messages.
    func setupMIDI() {
        // status holds the result of MIDI operations. OSStatus is a type used across
        // various system frameworks to represent the success or failure of an operation.
        var status: OSStatus
        
        // Create a MIDI Client. MIDIClientCreate creates a new client in the MIDI system.
        // "MIDIClient" is the name given to this client. If the client is successfully
        // created, its reference is stored in midiClient. The status is checked for errors.
        status = MIDIClientCreate("MIDIClient" as CFString, nil, nil, &midiClient)
        checkError(status)
        
        // Define the callback as a MIDIReadBlock. A MIDIReadBlock is called whenever
        // MIDI messages are received. Here, the callback calls processMIDIPackets,
        // which you'll define to handle the incoming MIDI data.
        let inputPortCallback: MIDIReadBlock = { [weak self] (pktList, srcConnRefCon) in
            self?.processMIDIPackets(pktList)
        }
        
        // Create an input port with the callback block. MIDIInputPortCreateWithBlock
        // creates a new input port for the midiClient, enabling it to receive MIDI messages.
        // "InputPort" is the name given to this port. If the port is successfully created,
        // its reference is stored in inPort. The status is checked for errors.
        status = MIDIInputPortCreateWithBlock(midiClient, "InputPort" as CFString, &inPort, inputPortCallback)
        checkError(status)
        
        // Connect the input port to all available sources. This loop iterates through all
        // MIDI sources (external devices or software) and connects them to the input port
        // so that MIDI messages from these sources will be delivered to inputPortCallback.
        let sourceCount = MIDIGetNumberOfSources()
        for i in 0..<sourceCount {
            let src = MIDIGetSource(i)
            MIDIPortConnectSource(inPort, src, nil)
        }
    }
    
    
    // The processMIDIPackets function is responsible for handling incoming MIDI packets.
    // It is called whenever new MIDI data is received by the input port.
    func processMIDIPackets(_ pktList: UnsafePointer<MIDIPacketList>) {
        // Access the first MIDI packet in the received packet list. The packet list
        // can contain one or more MIDI packets depending on how much data is being sent.
        var packet = pktList.pointee.packet
        
        // Iterate over each packet in the packet list. The number of packets is
        // determined by pktList.pointee.numPackets.
        for _ in 0..<pktList.pointee.numPackets {
            // Safely access the data bytes in the MIDI packet. withUnsafeBytes provides
            // temporary access to the memory where packet.data is stored.
            withUnsafeBytes(of: &packet.data) { dataPtr in
                // Bind the memory to UInt8 (the type used for MIDI bytes) and get a pointer
                // to the start of the data.
                let data = dataPtr.bindMemory(to: UInt8.self)
                guard let bytes = data.baseAddress else { return }
                
                // Variables to hold the values extracted from the MIDI message.
                var ccValue: UInt8 = 0
                var ccNumber: UInt8 = 0
                var channel: UInt8 = 0
                var dataIndex = 0
                
                // Iterate over each byte in the packet data. The packet.length tells us
                // how many bytes are in this packet.
                for i in 0..<Int(packet.length) {
                    // Access each byte in the packet. The advanced(by:) function moves
                    // the pointer by 'i' positions to access the correct byte.
                    let b = bytes.advanced(by: i).pointee
                    
                    // MIDI messages have a specific structure. The first byte (status byte)
                    // tells us the type of message and the channel. Subsequent bytes are data bytes.
                    // Here, we're parsing a Control Change message which consists of three bytes:
                    // 1. Status byte (indicates Control Change and channel)
                    // 2. Controller number (which control is being changed)
                    // 3. Controller value (the new value for the control)
                    if dataIndex == 0 {
                        channel = b & 0x0F  // Extract the channel (lower 4 bits of the status byte)
                    } else if dataIndex == 1 {
                        ccNumber = b  // The controller number
                    } else if dataIndex == 2 {
                        ccValue = b  // The controller value
                    }
                    dataIndex += 1
                    
                    // Once we've read enough bytes for a Control Change message, process it.
                    if dataIndex > 2 {
                        // Check if this is the specific control change message we're interested in.
                        // Here we're looking for CC#13 on channel 2.
                        if ccNumber == 13 && channel == 1 { // Remember channels are 0-indexed
                            // Update the noiseIntensity on the main thread because it's a @Published
                            // property and might trigger UI updates.
                            
                            DispatchQueue.main.async {
                                // Scale the MIDI value (0-127) to the slider's range (0.01-100)
                                // and update the noiseIntensity property.
                                self.noiseIntensity = Float(ccValue) / 127.0 * 1.0
                            }
                        }
                        // Break after processing one MIDI message. If there are more messages,
                        // they will be handled in subsequent iterations of the outer for loop.
                        break
                    }
                }
            }
            // Move to the next packet in the list for the next iteration of the loop.
            packet = MIDIPacketNext(&packet).pointee
        }
    }
    
    // The checkError function is a utility to help with debugging. It checks the status
    // returned by MIDI functions and prints an error message if something went wrong.
    func checkError(_ status: OSStatus) {
        // OSStatus is a type commonly used in Apple's C-based frameworks. A status of noErr (0)
        // indicates success. Any other value indicates some kind of error.
        if status != noErr {
            // Print the error status. In a robust application, you might want to do more
            // sophisticated error handling, possibly translating the OSStatus into a
            // human-readable message or taking different actions based on the error.
            print("MIDI Error: \(status)")
        }
    }
    
}
    
    
    
    
    
    
    
    
    
struct ContentView: View {
    @StateObject var midiManager = MidiManager()
    
    var body: some View {
        VStack {
            MetalView(noiseIntensity: $midiManager.noiseIntensity)
                .edgesIgnoringSafeArea(.all) // Ensure it covers the full screen
        }
        .background(Color.white) // Set the SwiftUI background to white
    }
}

    
    
    
    
    
    
    
    
    
    
    // MetalView is a struct that conforms to UIViewRepresentable. It's used to wrap a UIKit view (MTKView)
    // for use within a SwiftUI view hierarchy. This allows you to integrate Metal-based rendering into SwiftUI.
    struct MetalView: UIViewRepresentable {
        // @Binding creates a two-way connection between a property that stores data, and a view that displays
        // and changes the data. Here, noiseIntensity is bound to a Float value provided by the parent view,
        // allowing this view to read and write the value. This is particularly useful for reflecting changes
        // made through MIDI input in the UI (e.g., a slider) and vice versa.
        @Binding var noiseIntensity: Float  // Binding to the slider's value
        
        // makeUIView is a required method of UIViewRepresentable. It's where you create and configure
        // the UIKit view you want to display. Context is a struct containing information about the current
        // state of the system, including a coordinator for managing interactions with the UIKit view.
        func makeUIView(context: Context) -> MTKView {
            let mtkView = MTKView()  // Create a new instance of MTKView, a view for Metal rendering.
            mtkView.device = MTLCreateSystemDefaultDevice()  // Set the Metal device to the default device.
            mtkView.delegate = context.coordinator  // Assign the view's delegate to the coordinator defined below.
            mtkView.preferredFramesPerSecond = 60  // Set the preferred frame rate to 60 frames per second.
            mtkView.isPaused = false  // Ensure the view does not start in a paused state.
            return mtkView  // Return the configured MTKView instance.
        }
        
        // updateUIView is another required method of UIViewRepresentable. It's called when the view needs
        // to update its configuration or when SwiftUI state changes. This is where you would typically
        // make changes to the MTKView in response to changes in your app's state.
        func updateUIView(_ uiView: MTKView, context: Context) {
            // Perform any necessary updates to the MTKView when the view's state changes.
            // For instance, you might adjust rendering parameters based on app state.
        }
        
        // makeCoordinator is a method you can implement to create a custom coordinator
        // object for your UIViewRepresentable. Coordinators are used to communicate and
        // coordinate between the SwiftUI view and the UIKit view. It's especially useful for
        // handling delegate methods and other interactions with the UIKit view.
        func makeCoordinator() -> Coordinator {
            Coordinator(self, noiseIntensity: $noiseIntensity)  // Create a new Coordinator instance.
        }
        
        // Coordinator is a custom class that acts as a delegate and coordinator for the MTKView.
        // It conforms to NSObject to interact with UIKit and MTKViewDelegate for Metal rendering callbacks.
        class Coordinator: NSObject, MTKViewDelegate {
            var parent: MetalView  // Reference back to the MetalView struct.
            var device: MTLDevice!  // The Metal device used for rendering. Implicitly unwrapped because it's set in setupMetal.
            var commandQueue: MTLCommandQueue!  // Queue to hold and manage submitted commands to be executed by the GPU.
            var pipelineState: MTLRenderPipelineState!  // Compiled pipeline state object is used to configure the GPU for rendering.
            var pointBuffer: MTLBuffer!  // Buffer to hold vertex data.
            var numberOfPoints: Int = 0  // Number of points (vertices) to be rendered.
            var time: Float = 0  // A variable to keep track of time, useful for animations and time-based effects.
            @Binding var noiseIntensity: Float  // Binding to the noise intensity, reflecting changes made via MIDI or UI.
            
            // The Coordinator's initializer. It's called when the makeCoordinator method of MetalView is invoked.
            init(_ parent: MetalView, noiseIntensity: Binding<Float>) {
                self.parent = parent
                self._noiseIntensity = noiseIntensity  // Initialize the _noiseIntensity property with the binding.
                super.init()
                setupMetal()  // Configure the Metal environment.
                setupPoints()  // Set up the points (vertices) to be rendered.
            }
            
            
            
            
            
            
            
            // setupMetal is responsible for initializing the Metal environment needed for rendering.
            // This includes setting up the device, command queue, shader functions, and pipeline state.
            func setupMetal() {
                // Attempt to create a system default Metal device. A Metal device is an abstraction of the GPU
                // that allows you to interact with and send commands to it. If a device cannot be created,
                // it likely means Metal is not supported on the current hardware, and the app cannot proceed.
                guard let device = MTLCreateSystemDefaultDevice() else {
                    fatalError("Metal is not supported on this device")
                }
                self.device = device  // Store the Metal device for later use.
                
                // Create a command queue for the device. A command queue is used to organize the commands
                // you want the GPU to execute. Commands will be submitted to this queue later in the rendering process.
                commandQueue = device.makeCommandQueue()
                
                // Retrieve the default library of shader functions. These functions are typically defined
                // in a separate .metal file in your project. This library is where those functions are compiled and stored.
                let library = device.makeDefaultLibrary()
                // Retrieve the vertex shader function named 'vertex_main' from the library. The vertex shader
                // is responsible for processing individual vertices in your render pipeline.
                let vertexFunction = library?.makeFunction(name: "vertex_main")
                // Retrieve the fragment shader function named 'fragment_main'. The fragment shader is
                // responsible for determining the color of each pixel in the rendered output.
                let fragmentFunction = library?.makeFunction(name: "fragment_main")
                
                // Create a pipeline descriptor to configure the render pipeline. The pipeline state is what
                // combines the shaders and other configuration into a format the GPU can use.
                let pipelineDescriptor = MTLRenderPipelineDescriptor()
                pipelineDescriptor.vertexFunction = vertexFunction  // Assign the vertex function.
                pipelineDescriptor.fragmentFunction = fragmentFunction  // Assign the fragment function.
                // Configure the pixel format for the color attachments (the output of the render pipeline).
                // .bgra8Unorm is a common format where each pixel has a Blue, Green, Red, and Alpha component,
                // each occupying 8 bits.
                pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
                
                // Attempt to create the pipeline state with the given descriptor. If this fails,
                // it typically means there's an issue with the shader functions or other configuration.
                do {
                    pipelineState = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
                } catch let error {
                    // If creating the pipeline state fails, print the error and crash the app. In a real-world
                    // application, you might handle this more gracefully.
                    fatalError("Failed to create pipeline state: \(error)")
                }
            }
            
            
            
            
            
            
            // setupPoints is responsible for initializing the data for points (vertices) that will be rendered.
            // It calculates the positions of points in a grid and stores them in a buffer that the GPU can access.
            func setupPoints() {
                // Define the size of the grid. The grid will consist of gridSize x gridSize points.
                let gridSize: Int = 1600
                // Define the spacing between points. This determines how far apart each point is in the grid.
                let spacing: Float = 0.002
                // Initialize an empty array to hold the point data. SIMD2<Float> is a 2-component vector
                // type provided by Swift, which is suitable for storing 2D coordinates (x, y).
                var points: [SIMD2<Float>] = []
                
                // Iterate over each position in the grid to calculate the x and y coordinates for each point.
                for i in 0..<gridSize {
                    for j in 0..<gridSize {
                        // Calculate the x coordinate. It's based on the column index (i), spacing, and adjusted
                        // so that the points are centered around the origin (0,0).
                        let x = Float(i) * spacing - (spacing * Float(gridSize) / 2)
                        // Calculate the y coordinate. It's based on the row index (j), spacing, and adjusted
                        // in a similar way to x so that the points are centered.
                        let y = Float(j) * spacing - (spacing * Float(gridSize) / 2)
                        // Create a 2D point with the calculated x and y coordinates and add it to the points array.
                        points.append(SIMD2<Float>(x, y))
                    }
                }
                
                // Store the number of points calculated. This will be used later when instructing the GPU
                // how many points to render.
                numberOfPoints = points.count
                // Create a Metal buffer to hold the points data. Metal buffers are chunks of memory
                // managed by Metal that can store any kind of data the GPU needs to access.
                // Here, bytes: points provides the data to store, length: specifies how big the buffer needs
                // to be, and options: specifies memory storage modes and other options (if needed).
                pointBuffer = device.makeBuffer(bytes: points, length: MemoryLayout<SIMD2<Float>>.size * numberOfPoints, options: [])
            }
            
            
            
            
            
            
            
            
            // draw(in:) is a method called whenever the MTKView is ready to render a new frame.
            // It sets up the necessary Metal state and submits draw commands to the GPU.
            func draw(in view: MTKView) {
                // Ensure all required objects are available before proceeding with the drawing.
                // This includes a drawable to render into, a pipeline state, a command buffer to queue commands,
                // and a descriptor defining the render pass's configuration.
                guard let drawable = view.currentDrawable,
                      let pipelineState = pipelineState,
                      let commandBuffer = commandQueue.makeCommandBuffer(),
                      let renderPassDescriptor = view.currentRenderPassDescriptor else {
                    return  // If any of these are missing, exit early.
                }
                
                // Update the time variable based on the frame rate. This can be used for animations and
                // time-based effects in your shaders.
                time += 1 / Float(view.preferredFramesPerSecond)
                
                // Create a buffer to hold the time value. This buffer will be passed to the GPU so that
                // the shaders can use the current time value.
                var timeBuffer = time
                let timeBufferPointer = device.makeBuffer(bytes: &timeBuffer, length: MemoryLayout<Float>.size, options: [])
                // Similarly, create a buffer for the noiseIntensity value. This allows the shaders to access
                // the current noise intensity, which can be controlled via MIDI or a UI element.
                let noiseIntensityBufferPointer = device.makeBuffer(bytes: &_noiseIntensity.wrappedValue, length: MemoryLayout<Float>.size, options: [])
                
                // Configure the render pass. This setup specifies that the framebuffer should be cleared
                // to a black color before rendering.
                renderPassDescriptor.colorAttachments[0].loadAction = .clear
                renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0, 0, 0, 1)
                
                // Create a render command encoder. This object is used to record rendering commands.
                if let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor) {
                    // Set the current pipeline state object. This determines how vertices and fragments are processed.
                    renderEncoder.setRenderPipelineState(pipelineState)
                    // Bind the pointBuffer to the vertex shader. This provides the shader with the point data to render.
                    renderEncoder.setVertexBuffer(pointBuffer, offset: 0, index: 0)
                    // Bind the time buffer to the vertex shader.
                    renderEncoder.setVertexBuffer(timeBufferPointer, offset: 0, index: 1)
                    // Bind the noise intensity buffer to the vertex shader.
                    renderEncoder.setVertexBuffer(noiseIntensityBufferPointer, offset: 0, index: 2)
                    
                    // Issue a draw call to render the points. This tells the GPU to render a certain number of points
                    // using the currently bound vertex data and pipeline state.
                    renderEncoder.drawPrimitives(type: .point, vertexStart: 0, vertexCount: numberOfPoints)
                    // Finalize the encoding of drawing commands.
                    renderEncoder.endEncoding()
                    // Schedule a presentation of the drawable to the screen.
                    commandBuffer.present(drawable)
                }
                
                // Commit the command buffer. This submits the encoded commands to the GPU for execution.
                commandBuffer.commit()
            }
            
            // mtkView(_:drawableSizeWillChange:) is a delegate method that responds to changes in the drawable's
            // size, such as when the device's orientation changes or the view is resized. This can be important
            // for adjusting rendering parameters or reconfiguring your shaders.
            func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
                // Respond to drawable size changes if necessary. For example, you might want to update
                // the projection matrix in your shaders to accommodate the new aspect ratio.
            }
        }
    }
